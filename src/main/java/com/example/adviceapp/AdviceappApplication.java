package com.example.adviceapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdviceappApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdviceappApplication.class, args);
	}

}
